/*
SlugChess Core is a chess program for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <map>
#include <vector>
#include <list>
#include <set>
#include <iostream>
#include <sstream>
#include <algorithm>
#include "visionrules.h"
#include "sfen.h"
#include "san.h"

#include "shared.h"

class SlugChess {
public:
    enum EndResult{
        StillPlaying = 0,
        Draw = 1,
        WhiteWin = 2,
        BlackWin = 3,
    };
    enum Perspective{
        White = 0,
        Black = 1,
        Both = 2,
    };

    static std::string license_text;

    SlugChess(const std::string& sfenString, const VisionRules& visionRules);

    void DoMove(const std::string& from, const std::string& to);
    bool DoSanMove(const std::string& san_move);

    void PrintBoard(std::stringstream& ss, bool whitePlayer); 
    void PrintDebugBoard(std::stringstream& ss);
    void PrintWhiteVision(std::stringstream& ss);
    void PrintBlackVision(std::stringstream& ss);
    void PrintVisionBoard(std::stringstream& ss, bool visionBoard[]);
    void PrintSanMoves(std::stringstream& ss);
    void PrintLicense(std::stringstream& ss);

    std::string From(Perspective perspective);
    std::string To(Perspective perspective);

    std::vector<bool> GetWhiteVision(){ return std::vector<bool>(std::begin(_whiteVision), std::end(_whiteVision)); }
    std::vector<bool> GetBlackVision(){ return std::vector<bool>(std::begin(_blackVision), std::end(_blackVision)); }
    std::vector<ChessPiece> GetPieces(){std::vector<ChessPiece> pieces(64);for (int i = 0; i < 64; i++)pieces[i] = _board[i].Piece; return pieces; }
    void SetEnd(EndResult endResult){ _gameEnd = endResult; }
    EndResult Result(){ return _gameEnd; }

    bool LegalMove(const std::string& from, const std::string& to);

    std::map<int, std::vector<int>>* LegalMovesRef(){ return WhitesTurn()? &_legalWhiteMoves : &_legalBlackMoves; }
    std::map<int, std::vector<int>>* LegalWhiteMovesRef(){ return &_legalWhiteMoves; }
    std::map<int, std::vector<int>>* LegalBlackMovesRef(){ return &_legalBlackMoves; }
    std::map<int, std::vector<int>>* ShadowWhiteMovesRef(){ return &_shadowWhiteMoves; }
    std::map<int, std::vector<int>>* ShadowBlackMovesRef(){ return &_shadowBlackMoves; }

    std::set<int> Checks(Perspective perspective);

    bool WhitesTurn(){return _whiteTurn; }
    ChessPiece LastCaptured(){ return _lastCaptured; }
    std::list<std::pair<ChessPiece,int>>* KilledPieces(){ return &_killedPieces; }
    // "-" if there is no an passant
    const std::string GetAnPassant() {for (auto&& field : _board) {if(field.AnPassan_able){ return *field.fieldname; } }return "-"; }
    const std::string GetFenString() { return _fenString;}
    const std::string GetCurrentFenString();
    const std::string ResultString() { return _gameEnd==0?"*": _gameEnd==1? "1/2-1/2": _gameEnd==2?"1-0":"0-1";}


    static bool visionBoardTrue [64];
    const static VisionRules VisionRules_NoRules;
    const static VisionRules VisionRules_Torch;
    const static VisionRules VisionRules_Sight;
    static std::map<std::string, const VisionRules*> GetVisionRules();
    static std::vector<std::string> GetVariants();
    static std::vector<std::string> GetVariantsAsInSAN();
    static const VisionRules* GetVisionRule(const std::string& vrName);
    static const int32_t BPToIndx(const std::string& pos){return GameRules::BoardPosToIndex(pos);}
    static std::string BP(int index) { return GameRules::BoardPos(index); }
    
private:
    SlugChess() {}; //Hiding default constructor
    Field ExecuteMove(const std::string from, const std::string to);
    Field ExecuteMove(int from, int to);
    //void WriteLan(const std::string& from, const std::string& to);
    void PrintBoard(std::stringstream& ss, bool visionBoard[]);
    void PrintDebugBoard(std::stringstream& ss, bool visionboard[]);
    void WriteMoveSan(const std::string& from, const std::string& to);
    void CalculateVision();
    void CalculateLegalMoves();
    void CalculateLegalShadowMoves();
    void CalPossibleCastles();
    void ClearBlockedCastles();
    void CleanAnPassants();
    void FindChecks();
    int GetFieldWithPiece(ChessPiece piece);
    bool* VisionBoardPerspective(Perspective perspective);

 
    static void CalculateLegalMoves(std::vector<Field>& board, bool visionBoard[]);


    std::string _fenString;
    std::list<std::pair<int, int>> _moves;
    std::stringstream _sanMoves;
    EndResult _gameEnd;
    int _halfTurnSinceCaptureOrPawnMove = 0;
    int _turn = 1;
    bool _whiteTurn;
    VisionRules _rules; 
    std::vector<Field> _board;
    bool _whiteVision [64];
    bool _blackVision [64];
    int _lastCaptureField = -1;
    //std::map<int, std::vector<int>> _legalMoves;
    std::map<int, std::vector<int>> _legalWhiteMoves;
    std::map<int, std::vector<int>> _legalBlackMoves;
    std::map<int, std::vector<int>> _shadowWhiteMoves;
    std::map<int, std::vector<int>> _shadowBlackMoves;
    // Starts with king and ends with rook
    std::list<int> _possibleCastles;
    std::vector<std::vector<int32_t>> _whiteCastelingOpenFields;
    std::vector<std::vector<int32_t>> _blackCastelingOpenFields;
    std::list<std::pair<ChessPiece,int>> _killedPieces; //chesspiece and postion it died in
    ChessPiece _lastCaptured = ChessPiece::Non;
    std::list<int> _blackFieldsThatCheck;
    std::list<int> _whiteFieldsThatCheck;
};
