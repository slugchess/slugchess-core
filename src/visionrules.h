/*
SlugChess Core is a chess program for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <map>
#include "field.h"

struct Rules{
    bool ViewMoveFields;
    bool ViewCaptureField;
    int ViewRange;
    Rules(bool viewMoveFields, bool viewCaptureField,int viewRange){
        ViewMoveFields = viewMoveFields;
        ViewCaptureField = viewCaptureField;
        ViewRange = viewRange;
    }

    Rules(int range) : Rules(false, false, range){
        
    }

    Rules() : Rules(false, false, 0){
        
    }
};

struct VisionRules{
    bool enabled;
    Rules globalRules;
    std::map<ChessPiece, Rules> overWriteRules;
};
