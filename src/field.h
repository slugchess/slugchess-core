/*
SlugChess Core is a chess program for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <vector>

enum ChessPiece {
    Non = 0,
    BlackPawn = 1,
    BlackKnight = 2,
    BlackBishop = 3,
    BlackRook = 4,
    BlackQueen = 5,
    BlackKing = 6,
    WhitePawn = 7,
    WhiteKnight = 8,
    WhiteBishop = 9,
    WhiteRook = 10,
    WhiteQueen = 11,
    WhiteKing = 12
};

struct Field {
    public:
    const std::string* fieldname; //pointer to string in BoardPos
    bool AnPassan_able;
    bool FirstMove;
    bool PieceCapturedLastMove;
    ChessPiece Piece;

    Field(ChessPiece piece, bool anPassanAble, bool firstMove, bool pieceCapturedLastMove)
    {
        Piece = piece;
        AnPassan_able = anPassanAble;
        FirstMove = firstMove; //first move means an passant creating if pawn and castleable if rook or king
        PieceCapturedLastMove = pieceCapturedLastMove;
    }

    Field(ChessPiece piece) : Field(piece, false, false, false)
    {

    }

    Field() : Field(ChessPiece::Non)
    {

    }

    Field(ChessPiece newPiece, const std::string* samefieldname) : Field(newPiece)
    {
        fieldname = samefieldname;
    }

    bool inline HasBlackPiece()
    {
       return Piece > Non && !HasWhitePiece();
    }

    bool inline HasWhitePiece()
    {
        return Piece > BlackKing;
    }

    static int IndexRow(int pos)
    {
        return (pos/8);
    }

    static int IndexColumn(int pos)
    {
        return (pos%8);
    }

    static bool BlackPiece(ChessPiece piece)
    {
        return piece > Non && !WhitePiece(piece);
    }

    static bool WhitePiece(ChessPiece piece)
    {
        return piece > BlackKing;
    }

    static bool IsPawn(Field field)
    {
        return field.Piece == ChessPiece::WhitePawn || field.Piece == ChessPiece::BlackPawn;
    }

    static bool IsKing(Field field)
    {
        return field.Piece == ChessPiece::WhiteKing || field.Piece == ChessPiece::BlackKing;
    }

    static char PieceChar(ChessPiece piece)
    {
        switch (piece)
        {
            case WhiteKing:
                return 'K';
            case WhiteQueen:
                return 'Q';
            case WhiteBishop:
                return 'B';
            case WhiteKnight:
                return 'N';
            case WhiteRook:
                return 'R';
            case WhitePawn:
                return 'P';
            case BlackKing:
                return 'k';
            case BlackQueen:
                return 'q';
            case BlackBishop:
                return 'b';
            case BlackKnight:
                return 'n';
            case BlackRook:
                return 'r';
            case BlackPawn:
                return 'p';
            case Non:
                return ' ';
            default:
                return 'E';
        }
    }

    static char PieceCharCapital(ChessPiece piece)
    {
        switch (piece)
        {
            case WhiteKing:
                return 'K';
            case WhiteQueen:
                return 'Q';
            case WhiteBishop:
                return 'B';
            case WhiteKnight:
                return 'N';
            case WhiteRook:
                return 'R';
            case WhitePawn:
                return 'P';
            case BlackKing:
                return 'K';
            case BlackQueen:
                return 'Q';
            case BlackBishop:
                return 'B';
            case BlackKnight:
                return 'N';
            case BlackRook:
                return 'R';
            case BlackPawn:
                return 'P';
            case Non:
                return ' ';
            default:
                return 'E';
        }
    }
};

