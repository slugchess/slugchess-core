/*
SlugChess Core is a chess program for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <vector>
#include <locale>
#include <cwctype>
#include <locale>
#include <string>
#include <list>
#include <random>
#include <chrono>
#include <algorithm>  
#include "gamerules.h"


class Sfen{
    public:
    static const std::string GenNormalChess()
    {
        return "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1";
    }

    static const std::string GenSlugRandom()
    {
        std::vector<ChessPiece> bagOfBlack = {ChessPiece::BlackKing, ChessPiece::BlackQueen, ChessPiece::BlackRook, ChessPiece::BlackRook, ChessPiece::BlackBishop, ChessPiece::BlackBishop, ChessPiece::BlackKnight, ChessPiece::BlackKnight};
        std::vector<ChessPiece> bagOfWhite = {ChessPiece::WhiteKing, ChessPiece::WhiteQueen, ChessPiece::WhiteRook, ChessPiece::WhiteRook, ChessPiece::WhiteBishop, ChessPiece::WhiteBishop, ChessPiece::WhiteKnight, ChessPiece::WhiteKnight};
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        auto rand = std::default_random_engine(seed);
        shuffle(bagOfBlack.begin(), bagOfBlack.end(), rand); 
        shuffle(bagOfWhite.begin(), bagOfWhite.end(), rand);
        std::stringstream ss;
         std::for_each(bagOfBlack.begin(), bagOfBlack.end(), [&ss](ChessPiece& piece){
            ss << Field::PieceChar(piece);
        });
        ss << "/pppppppp/8/8/8/8/PPPPPPPP/";
        std::for_each(bagOfWhite.begin(), bagOfWhite.end(), [&ss](ChessPiece& piece){
           ss << Field::PieceChar(piece);
        });
        ss << " w - - 0 1";
        return ss.str();
    }

    /**
     * True if succsessful. Only support start 0 moves done
     * */
    static int WriteSfenPiecesToBoard(std::vector<Field>& board, const std::string& sfen)
    {
        // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1
        // normal startingpoint shredder-fen

        //Field(ChessPiece piece, bool anPassanAble, bool firstMove, bool pieceCapturedLastMove)
        int fenIndex = 0;
        int columnIndex = 0;
        int rowindex = 7;
        bool piecesLoop = true;
        while (piecesLoop)
        {
            char character = sfen[fenIndex];
            //std::cout << "fenchar  " << character << std::endl;
            switch (character)
            {
            case '/':
                columnIndex = 0;
                rowindex--;
                break;
            case ' ':
                piecesLoop = false;
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
                columnIndex+= character - '0';
                break;
            case 'p':
                if(rowindex == 6){
                    board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackPawn, false, true, false);
                }else{
                    board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackPawn, false, false, false);
                }
                columnIndex++;
                break;
            case 'P':
                if(rowindex == 1){
                    board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhitePawn, false, true, false);
                }else{
                    board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhitePawn, false, false, false);
                }
                columnIndex++;
                break;
            case 'n':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackKnight);
                columnIndex++;
                break;
            case 'N':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhiteKnight);
                columnIndex++;
                break;
                case 'b':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackBishop);
                columnIndex++;
                break;
            case 'B':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhiteBishop);
                columnIndex++;
                break;
            case 'q':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackQueen);
                columnIndex++;
                break;
            case 'Q':
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhiteQueen);
                columnIndex++;
                break;
            case 'k': 
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackKing);
                columnIndex++;
                break;
            case 'K': 
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhiteKing);
                columnIndex++;
                break;
            case 'r': 
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::BlackRook);
                columnIndex++;
                break;
            case 'R': 
                board[BIdx(columnIndex, rowindex)] = Field(ChessPiece::WhiteRook);
                columnIndex++;
                break;
            default:
                return false;
            }
            fenIndex++;
        }
        // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1
        fenIndex += 2; //ignore whose move it is
        bool castleLoop = true;
        while (castleLoop)
        {
            char character = sfen[fenIndex];
            //std::cout << "fenchar cast pos " << std::string(1,std::tolower(character))+'1' << std::endl;
            if(character == '-'){
                fenIndex++;
                castleLoop = false;
            }else if( character == ' '){
                castleLoop = false;
            }else if(character >= 'A' && character <= 'H'){
                int posIndex = GameRules::BoardPosToIndex(std::string(1,std::tolower(character, std::locale()))+'1');
                //std::cout << "fenchar cast posIndex  " << std::to_string(posIndex) << std::endl;
                if(board[posIndex].Piece == ChessPiece::WhiteRook){
                    for (auto&& field : board)
                    {
                        if(field.Piece == ChessPiece::WhiteKing){
                            field.FirstMove = true;
                        }
                    }
                    board[posIndex].FirstMove = true;
                }
            }else if(character >= 'a' && character <= 'h'){
                int posIndex = GameRules::BoardPosToIndex(std::string(1,std::tolower(character, std::locale()))+'8');
                if(board[posIndex].Piece == ChessPiece::BlackRook){
                    for (auto&& field : board)
                    {
                        if(field.Piece == BlackKing){
                            field.FirstMove = true;
                        }
                    }
                    board[posIndex].FirstMove = true;
                }
            }
            fenIndex++;
        }

        if(sfen[fenIndex] != '-'){
            //std::cout << "fenchar cast anpass " << std::string(1,sfen[fenIndex]) + std::string(1,sfen[fenIndex+1]) << std::endl;
            board[GameRules::BoardPosToIndex(sfen.substr(fenIndex, 2))].AnPassan_able = true;
            fenIndex+= 2;
        }else{
            fenIndex++;
        }

        //Set fieldName
        for(int i = 0; i < 64; i++)
        {
            board[i].fieldname = GameRules::BoardPosRef(i);
        }
        
        return true;

    }

    private:
    static int BIdx(int columIndex, int rowIndex){
        return GameRules::IndexFromColRow(columIndex, rowIndex);
    }

};