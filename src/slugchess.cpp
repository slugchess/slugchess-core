/*
SlugChess Core is a chess program for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "slugchess.h"

#include <iterator>
#ifdef WIN
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#endif

bool SlugChess::visionBoardTrue[] = {
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true,
    true, true, true, true, true, true, true, true};

const VisionRules SlugChess::VisionRules_NoRules = VisionRules{ 
    false, 
    {false, false, 0},
    {  }
};
const VisionRules SlugChess::VisionRules_Torch = VisionRules{ 
    true, 
    {false, true, 2},
    {
        {ChessPiece::WhitePawn, Rules{false, true, 1}},
        {ChessPiece::BlackPawn, Rules{false, true, 1}},
        {ChessPiece::WhiteKing, Rules{false, true, 1}},
        {ChessPiece::BlackKing, Rules{false, true, 1}},
    }
};
const VisionRules SlugChess::VisionRules_Sight = VisionRules{ 
    true, 
    {true, true, 0},
    {
        {ChessPiece::WhitePawn, Rules{true, true, 1}},
        {ChessPiece::BlackPawn, Rules{true, true, 1}},
        {ChessPiece::WhiteKnight, Rules{true, true, 1}},
        {ChessPiece::BlackKnight, Rules{true, true, 1}},
    }
};

std::map<std::string, const VisionRules*> SlugChess::GetVisionRules()
{
    return {{"NoRules", &VisionRules_NoRules}, {"Sight", &VisionRules_Sight}, {"Torch", &VisionRules_Torch}};
}
std::vector<std::string> SlugChess::GetVariants()
{
    return {"Classic", "Sight", "Torch"/*,"Custom"?*/,};
}
std::vector<std::string> SlugChess::GetVariantsAsInSAN()
{
    return {"SlugChess.Sight", "SlugChess.Torch"/*,"SlugChess.Custom"?*/,};
}
const VisionRules* SlugChess::GetVisionRule(const std::string& vrName)
{
    //std::cout << "Fetching vision " << vrName << std::endl;
    if(vrName == "Sight"){
        return &VisionRules_Sight;
    }else if(vrName == "Torch"){
        return &VisionRules_Torch;
    }else if(vrName == "SightWip"){
        return &VisionRules_Sight;
    }else if(vrName == "TorchWip"){
        return &VisionRules_Torch;
    }else if(vrName == "Classic"){
        return &VisionRules_NoRules;
    }else{
        return nullptr;
    }
}

SlugChess::SlugChess(const std::string& sfenString, const VisionRules& visionRules){
    // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1
    // normal startingpoint shredder-fen

#ifdef DEBUG                             
    std::cout << "SCC: Debug Mode" << std::endl;
#endif                                     
    _fenString = sfenString;
    _board = std::vector<Field>(64);
    _rules = visionRules;
    _killedPieces.clear();
    _whiteTurn = true;
    _gameEnd = EndResult::StillPlaying;
    _turn = 1;
    _halfTurnSinceCaptureOrPawnMove = 0;
    Sfen::WriteSfenPiecesToBoard(_board, sfenString);
    CalculateVision();
    CalculateLegalMoves();
    CalculateLegalShadowMoves();
    //CalPossibleCastles();
}


const std::string SlugChess::GetCurrentFenString(){
    // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1
    std::stringstream ss, resultSS;
    //Row is from bottom to top of board
    int row = 1, column = 1, emptyCount = 0;
    std::cout << "start" << std::endl;
    while (row < 9)
    {
        int i = column-1 + (row-1)*8;
        if(_board[i].Piece == ChessPiece::Non){
            emptyCount += 1;
            if(column == 8){
                ss << std::to_string(emptyCount);
                emptyCount = 0;
            }
        }else{
            if(emptyCount > 0){
                ss << std::to_string(emptyCount);
                emptyCount = 0;
            }
            ss << Field::PieceChar(_board[i].Piece);
        }
        if(column == 8){
            if(row > 1) ss << '/';
            //std::cout << ss.str() << std::endl;
            ss << resultSS.str();
            resultSS = std::move(ss);
            ss.str("");
            column = 1;
            row += 1;
        }else{
            column += 1;
        }
    }
    resultSS << ' ' << (_whiteTurn?'w':'b') << ' ';
    for(int i : _possibleCastles){
        resultSS << Field::PieceChar(_board[i].Piece);
    }
    if(_possibleCastles.size() == 0) resultSS << '-';
    resultSS << ' ' << GetAnPassant() << ' ' << std::to_string(_halfTurnSinceCaptureOrPawnMove)
        << ' ' << std::to_string(_turn);
    
	return resultSS.str();
}

void SlugChess::PrintBoard(std::stringstream& ss, bool whiteplayer){
    PrintBoard(ss, whiteplayer?_whiteVision:_blackVision);
}

void SlugChess::PrintBoard(std::stringstream& ss, bool visionboard[]){
    for(int row = 7; row >= 0;row--){
        for(int col = 0; col <= 7;col++){
            int index = GameRules::IndexFromColRow(col, row);
            if(visionboard[index]) {
                ss << "[ " << Field::PieceChar(_board[index].Piece) << " ]";

            }else{
                ss << "[ " << '#' << " ]";
            }
        }
        ss << std::endl;
    }   
}

void SlugChess::PrintDebugBoard(std::stringstream& ss){
    PrintDebugBoard(ss, visionBoardTrue);
}

void SlugChess::PrintDebugBoard(std::stringstream& ss, bool visionboard[]){
    for(int row = 7; row >= 0;row--){
        for(int col = 0; col <= 7;col++){
            int index = GameRules::IndexFromColRow(col, row);
            if(!visionboard[index]) continue;
            ss << "[ " << Field::PieceChar(_board[index].Piece) << " ]";
        }
        ss << std::endl;
    }
    ss << "Castling ";
    for (auto field : _board)
    {
        if(field.Piece == ChessPiece::WhiteKing && field.FirstMove){
            ss << "White ";
            for (auto field2 : _board)
            {
                if(field2.Piece == ChessPiece::WhiteRook && field.FirstMove){
                    ss << *field2.fieldname << " ";
                }
            }
        }
        if(field.Piece == ChessPiece::BlackKing && field.FirstMove){
            ss << "Black ";
            for (auto field2 : _board)
            {
                if(field2.Piece == ChessPiece::BlackRook && field.FirstMove){
                    ss << *(field2.fieldname) << " ";
                }
            }
        }
    }
    ss << std::endl;
    std::string anPass = GetAnPassant();
    ss << "An Passant: " << anPass << std::endl;
}

void SlugChess::PrintWhiteVision(std::stringstream& ss){
    for(int row = 7; row >= 0;row--){   
        for(int col = 0; col <= 7;col++){
            int index = GameRules::IndexFromColRow(col, row);
            if(_whiteVision[index]){
                ss << "[ " << Field::PieceChar(_board[index].Piece) << " ]";
            }else{
                ss << "[ " << "#"<< " ]";
            }
        }
        ss << std::endl;
    }
}

void SlugChess::PrintBlackVision(std::stringstream& ss){
    PrintVisionBoard(ss, _blackVision);
}

void SlugChess::PrintVisionBoard(std::stringstream& ss, bool visionBoard[]){
    for(int row = 7; row >= 0;row--){   
        for(int col = 0; col <= 7;col++){
            int index = GameRules::IndexFromColRow(col, row);
            if(visionBoard[index]){
                ss << "[ " << Field::PieceChar(_board[index].Piece) << " ]";
            }else{
                ss << "[ " << "#"<< " ]";
            }
        }
        ss << std::endl;
    }
}

void SlugChess::CalculateVision(){
    if (!_rules.enabled)
    {
        std::fill(std::begin(_whiteVision), std::end(_whiteVision), true);
        std::fill(std::begin(_blackVision), std::end(_blackVision), true);
        return;
    }
    std::fill(std::begin(_whiteVision), std::end(_whiteVision), false);
    std::fill(std::begin(_blackVision), std::end(_blackVision), false);
    std::vector<std::vector<int32_t>> dummy{};
    for (int i = 0; i < 64; i++)
    {
        if (_board[i].Piece == ChessPiece::Non) continue;
        GameRules::AddPieceVision(_board, 
                                    i, 
                                    _rules.overWriteRules.count(_board[i].Piece)>0? _rules.overWriteRules[_board[i].Piece] : _rules.globalRules
                                    , _board[i].HasWhitePiece()? _whiteVision:_blackVision);
        if (_rules.globalRules.ViewMoveFields)
        {
            //_legalMoves.clear();
            std::map<int, std::vector<int>> visionMoves;
            
            for(int i = 0; i < 64;i++){
                if(_board[i].HasWhitePiece()){
                    visionMoves[i];
                    GameRules::GetLegalMoves(visionMoves[i], _board, i, visionBoardTrue, dummy);
                }
            }
            for (auto &&keyVal : visionMoves)
            {
                for (auto &&to : keyVal.second){
                    _whiteVision[to] = true;
                }
            }
            visionMoves.clear();
            for(int i = 0; i < 64;i++){
                if(_board[i].HasBlackPiece()){
                    visionMoves[i];
                    GameRules::GetLegalMoves(visionMoves[i], _board, i, visionBoardTrue, dummy);
                }
            }
            for (auto &&keyVal : visionMoves)
            {
                for (auto &&to : keyVal.second){
                    _blackVision[to] = true;
                }
            }
        }
    }
    if (_rules.globalRules.ViewCaptureField && _lastCaptureField != -1)
    {
        _whiteVision[_lastCaptureField] = true;
        _blackVision[_lastCaptureField] = true;
    }
}

void SlugChess::CalculateLegalMoves(){
    _legalWhiteMoves.clear();
    _legalBlackMoves.clear();
    _whiteCastelingOpenFields.clear();
    _blackCastelingOpenFields.clear();
    for(int i = 0; i < 64;i++){
        if(_board[i].HasWhitePiece()){
            std::vector<int> movesI;
            GameRules::GetLegalMoves(movesI, _board, i, _whiteVision, _whiteCastelingOpenFields);
            if(movesI.size() > 0) _legalWhiteMoves[i] = movesI;
        }
    }
    for(int i = 0; i < 64;i++){
        if(_board[i].HasBlackPiece()){
            std::vector<int> movesI;
            GameRules::GetLegalMoves(movesI, _board, i, _blackVision, _blackCastelingOpenFields);
            if(movesI.size() > 0) _legalBlackMoves[i] = movesI;

        }
    }
}

void SlugChess::CalculateLegalShadowMoves(){
    _shadowWhiteMoves.clear();
    _shadowBlackMoves.clear();
    std::vector<Field> whiteBoard(_board);
    for(int i = 0; i < 64;i++){
        if(!_whiteVision[i]){
            whiteBoard[i] = Field();
        }
    }
    std::vector<Field> blackBoard(_board);
    for(int i = 0; i < 64;i++){
        if(!_blackVision[i]){
            blackBoard[i] = Field();
        }
    }
    for(int i = 0; i < 64;i++){
        if(whiteBoard[i].HasBlackPiece()){
            _shadowBlackMoves[i];
            //std::cout << "getting leagal moves for " << Field::PieceChar( _board[i].Piece) << *_board[i].fieldname << std::endl << std::flush;
            std::vector<std::vector<int32_t>> dummy{};
            GameRules::GetLegalMoves(_shadowBlackMoves[i], whiteBoard, i, visionBoardTrue, dummy);
            //std::cout << "found " << _legalShadowMoves[i].size() << std::endl << std::flush;
        }
    }
    for(int i = 0; i < 64;i++){
        if(blackBoard[i].HasWhitePiece()){
            _shadowWhiteMoves[i];
            //std::cout << "getting leagal moves for " << Field::PieceChar( _board[i].Piece) << *_board[i].fieldname << std::endl << std::flush;
            std::vector<std::vector<int32_t>> dummy{};
            GameRules::GetLegalMoves(_shadowWhiteMoves[i], blackBoard, i, visionBoardTrue, dummy);
            //std::cout << "found " << _legalShadowMoves[i].size() << std::endl << std::flush;
        }
    }
   
}

void SlugChess::ClearBlockedCastles(){

    auto blocked = GameRules::GetBlockedCastles(_whiteCastelingOpenFields, _legalBlackMoves, _board);
    for(auto& [front, back] : blocked){
        auto it = std::remove_if(_legalWhiteMoves.at(front).begin(), _legalWhiteMoves.at(front).end(), [back](auto& x){ return x == back; });
        _legalWhiteMoves.at(front).erase(it, _legalWhiteMoves.at(front).end());
    }
    blocked = GameRules::GetBlockedCastles(_blackCastelingOpenFields, _legalWhiteMoves, _board);
    for(auto& [front, back] : blocked){
        auto it = std::remove_if(_legalBlackMoves.at(front).begin(), _legalBlackMoves.at(front).end(), [back](auto& x){ return x == back; });
        _legalBlackMoves.at(front).erase(it, _legalBlackMoves.at(front).end());
    }

    blocked = GameRules::GetBlockedCastles(_whiteCastelingOpenFields, _shadowBlackMoves, _board);
    for(auto& [front, back] : blocked){
        auto it = std::remove_if(_shadowWhiteMoves.at(front).begin(), _shadowWhiteMoves.at(front).end(), [back](auto& x){ return x == back; });
        _shadowWhiteMoves.at(front).erase(it, _shadowWhiteMoves.at(front).end());
    }
    blocked = GameRules::GetBlockedCastles(_blackCastelingOpenFields, _shadowWhiteMoves, _board);
    for(auto& [front, back] : blocked){
        auto it = std::remove_if(_shadowBlackMoves.at(front).begin(), _shadowBlackMoves.at(front).end(), [back](auto& x){ return x == back; });
        _shadowBlackMoves.at(front).erase(it, _shadowBlackMoves.at(front).end());
    }
}

void SlugChess::FindChecks(){
    // _blackFieldsThatCheck.clear();
    // _whiteFieldsThatCheck.clear();
    // auto whiteMoves = _whiteTurn?_legalMovesWithFullVision:_legalShadowMovesWithFullVision;
    // auto blackMoves = _whiteTurn?_legalShadowMovesWithFullVision:_legalMovesWithFullVision;
    // int whiteKingIndex = 0;
    // for(; whiteKingIndex < 64; whiteKingIndex++){
    //     if(_board[whiteKingIndex].Piece == ChessPiece::WhiteKing)break;
    // }
    // int blackKingIndex = 0;
    // for(; blackKingIndex < 64; blackKingIndex++){
    //     if(_board[blackKingIndex].Piece == ChessPiece::BlackKing)break;
    // }
    // for (auto &&keyVal : whiteMoves)
    // {
    //     if(std::find(keyVal.second.begin(), keyVal.second.end(), blackKingIndex) != keyVal.second.end()){
    //         _whiteFieldsThatCheck.push_back(keyVal.first);
    //     }
    // }
    // for (auto &&keyVal : blackMoves)
    // {
    //     if(std::find(keyVal.second.begin(), keyVal.second.end(), whiteKingIndex) != keyVal.second.end()){
    //         _blackFieldsThatCheck.push_back(keyVal.first);
    //     }
    // }
    
}
std::set<int> SlugChess::Checks(Perspective perspective){
    std::set<int> set;
    if(perspective != Perspective::Both){
        bool whitePerspec = perspective == Perspective::White;
        //if(whitePerspec)std::cout << "!!!!white prespec check" << std::endl << std::flush;
        //if(!whitePerspec)std::cout << "!!!!black prespec check" << std::endl << std::flush;
        auto moves = whitePerspec?_legalWhiteMoves:_legalBlackMoves;
        auto shadowMoves = whitePerspec?_shadowBlackMoves:_shadowWhiteMoves;
        int kingPespective = GetFieldWithPiece(whitePerspec?ChessPiece::WhiteKing:ChessPiece::BlackKing);
        int kingOther = GetFieldWithPiece(whitePerspec?ChessPiece::BlackKing:ChessPiece::WhiteKing);
        for (auto &&keyVal : moves)
        {
            auto kingIter = std::find(keyVal.second.begin(), keyVal.second.end(), kingOther);
            if(kingIter != keyVal.second.end()){
                set.insert(keyVal.first);
                set.insert(*kingIter);
            }
        }
        for (auto &&keyVal : shadowMoves)
        {
            auto kingIter = std::find(keyVal.second.begin(), keyVal.second.end(), kingPespective);
            if(kingIter != keyVal.second.end()){
                set.insert(keyVal.first);
                set.insert(*kingIter);
            }
        }
        
    }else{ //This is a bit hacky function. Checks should be recorded better 
        auto moves = _whiteTurn?_legalWhiteMoves:_legalBlackMoves;
        auto shadowMoves = _whiteTurn?_shadowBlackMoves:_shadowWhiteMoves;
        int kingPespective = GetFieldWithPiece(_whiteTurn?ChessPiece::WhiteKing:ChessPiece::BlackKing);
        int kingOther = GetFieldWithPiece(_whiteTurn?ChessPiece::BlackKing:ChessPiece::WhiteKing);
        for (auto &&keyVal : moves)
        {
            auto kingIter = std::find(keyVal.second.begin(), keyVal.second.end(), kingOther);
            if(kingIter != keyVal.second.end()){
                set.insert(keyVal.first);
                set.insert(*kingIter);
            }
        }
        for (auto &&keyVal : shadowMoves)
        {
            auto kingIter = std::find(keyVal.second.begin(), keyVal.second.end(), kingPespective);
            if(kingIter != keyVal.second.end()){
                set.insert(keyVal.first);
                set.insert(*kingIter);
            }
        }
    }
    return set;
}

int SlugChess::GetFieldWithPiece(ChessPiece piece){
    for(int i = 0; i < 64;i++){
        if(_board[i].Piece == piece){
            return i;
        }
    }
    return -1;
}

void SlugChess::CalPossibleCastles(){
    _possibleCastles.clear();
    for(int i = 0; i < 64;i++){
        if(_board[i].Piece == ChessPiece::WhiteKing){
            if(_board[i].FirstMove){
                for(int j = 0; j < 64;j++){
                    if(_board[j].Piece == ChessPiece::WhiteRook && _board[j].FirstMove){
                        _possibleCastles.push_back(j);
                    }
                }
            }
        }
        if(_board[i].Piece == ChessPiece::BlackKing){
            if(_board[i].FirstMove){
                for(int j = 0; j < 64;j++){
                    if(_board[j].Piece == ChessPiece::BlackRook && _board[j].FirstMove){
                        _possibleCastles.push_back(j);
                    }
                }
            }
        }
    }
}

void SlugChess::CleanAnPassants()
    {
        //with test
        int index = 0;
        for(Field& field : _board)
        {
            if(field.AnPassan_able)
            {
                field.AnPassan_able = false;
                break;
            }
            index++;
        }
        if(index < 63 && _board[index].AnPassan_able){
            std::cout << "Clearing an passant failed" << std::flush << std::endl;
        }
    }


Field SlugChess::ExecuteMove(const std::string from, const std::string to){
    return ExecuteMove(GameRules::BoardPosToIndex(from), GameRules::BoardPosToIndex(to));
}

Field SlugChess::ExecuteMove(int from, int to){
    Field tofield = Field(_board[to].Piece, _board[to].fieldname);
    //int killedPos = -1;
    int modTo = to%8;
    int modFrom = from%8;
    //std::cout << "!!! Board modTo" << std::to_string(modTo) << "modFrom " << std::to_string(modFrom) << std::endl;
    //std::cout << "!!! Board from first move " << std::to_string(_board[from].FirstMove) << std::endl;
    //std::cout << "!!! Board from is king " << std::to_string(_board[from].Piece == ChessPiece::WhiteKing || _board[from].Piece == ChessPiece::BlackKing) << std::endl;
    //std::cout << "!!! Board mod diff 2 " << std::to_string(modTo == modFrom-2 || modTo == modFrom+2) << std::endl;
    if(_board[from].FirstMove && (_board[from].Piece == ChessPiece::WhiteKing || _board[from].Piece == ChessPiece::BlackKing) && (modTo > modFrom+1 || modTo < modFrom-1)){
        //std::cout << "!!!Dooing Castling" << std::endl;
        int kingEnd;
        int rookEnd;
        //ChessPiece rook = _board[to].HasWhitePiece()?ChessPiece::WhiteRook:ChessPiece::BlackRook;
        if(modTo > modFrom+1){
            kingEnd = GameRules::RightOne(GameRules::RightOne(from));
            rookEnd = GameRules::RightOne(from);
        }
        else if(modTo < modFrom-1)
        {
            kingEnd = GameRules::LeftOne(GameRules::LeftOne(from));
            rookEnd = GameRules::LeftOne(from);
        }
        _board[rookEnd] = Field(_board[to].Piece, _board[rookEnd].fieldname);
        _board[kingEnd] = Field(_board[from].Piece, _board[kingEnd].fieldname);
        _board[to] = Field(ChessPiece::Non, _board[to].fieldname);
        _board[from] = Field(ChessPiece::Non, _board[from].fieldname);
        tofield = Field(_board[to].Piece, _board[to].fieldname);
    }
    else if((_board[from].Piece == ChessPiece::WhitePawn || _board[from].Piece == ChessPiece::BlackPawn) && modFrom != modTo && _board[to].Piece == ChessPiece::Non){
        //preform an passant
        auto moveFunc = _board[from].HasWhitePiece()?GameRules::DownOne:GameRules::UpOne;
        int killedTo = moveFunc(to);
        tofield = Field(_board[killedTo].Piece, _board[killedTo].fieldname);
        _board[to] = Field(_board[from].Piece, _board[to].fieldname);
        _board[from] = Field(ChessPiece::Non, _board[from].fieldname);
        _board[killedTo] = Field(ChessPiece::Non, _board[killedTo].fieldname);
    }
    else
    {
        //std::cout << *_board[from].fieldname << " is from " << *_board[to].fieldname << " is to " << std::endl; 
        _board[to] = Field(_board[from].Piece, _board[to].fieldname);
        //std::cout << *_board[to].fieldname << " should now have " << Field::PieceChar(_board[from].Piece) << std::endl; 
        if(_board[to].Piece == ChessPiece::WhitePawn && Field::IndexRow(to) == 7)_board[to].Piece = ChessPiece::WhiteQueen;
        if(_board[to].Piece == ChessPiece::BlackPawn && Field::IndexRow(to) == 0)_board[to].Piece = ChessPiece::BlackQueen;
        if(_board[to].Piece == ChessPiece::WhitePawn && GameRules::DownOne(GameRules::DownOne(to)) == from)_board[GameRules::DownOne(to)].AnPassan_able = true;
        if(_board[to].Piece == ChessPiece::BlackPawn && GameRules::UpOne(GameRules::UpOne(to)) == from)_board[GameRules::UpOne(to)].AnPassan_able = true;
        _board[from] = Field(ChessPiece::Non, _board[from].fieldname);
        //std::cout << Field::PieceChar(_board[to].Piece) << " is on row " << std::to_string(Field::IndexRow(to)) << std::endl; 
        //std::cout << *_board[to].fieldname << " is now " << Field::PieceChar(_board[to].Piece) << std::endl; 
    }
    
    _moves.push_back(std::pair<int, int>(from, to));
    //Selected = null;
    return tofield;
}





bool SlugChess::LegalMove(const std::string& from, const std::string& to)
{
    if(from[0] < 'a' || from[0] > 'h' 
        ||from[1] < '1' || from[1] > '8'
        ||to[0] < 'a' || to[0] > 'h'
        ||to[1] < '1' || to[1] > '8' ) return false;
    auto& legalMoves = _whiteTurn?_legalWhiteMoves:_legalBlackMoves;
    if(legalMoves.count(BPToIndx(from)) > 0){
        std::vector<int> vector = legalMoves[BPToIndx(from)];
        return std::find(vector.begin(), vector.end(), BPToIndx(to)) != vector.end();
    }
    return false;
}

void SlugChess::DoMove(const std::string& from, const std::string& to){
    //WriteLan(from,to);
    CleanAnPassants();
    WriteMoveSan(from, to);
    Field attackedField = ExecuteMove(from, to);
    _whiteTurn = !_whiteTurn;
    if(_whiteTurn) _turn++;
    _lastCaptured = attackedField.Piece;
    if(attackedField.Piece != ChessPiece::Non){
        //std::cout << "Killed piece " << Field::PieceChar(attackedField.Piece) << " at " << *attackedField.fieldname << std::endl;
        _lastCaptureField = GameRules::BoardPosToIndex(*attackedField.fieldname);
        _killedPieces.push_back(std::pair<ChessPiece,int>(attackedField.Piece, GameRules::BoardPosToIndex(*attackedField.fieldname)));
        _halfTurnSinceCaptureOrPawnMove = 0;
    }else{
        _lastCaptureField = -1;
        _halfTurnSinceCaptureOrPawnMove++;
    }
    if(_lastCaptured == ChessPiece::WhiteKing){
        _gameEnd = EndResult::BlackWin;
    }
    if(_lastCaptured == ChessPiece::BlackKing){
        _gameEnd = EndResult::WhiteWin;
    }
    if(Field::IsPawn(_board[BPToIndx(to)])) _halfTurnSinceCaptureOrPawnMove = 0;
    CalculateVision();
    CalculateLegalMoves();
    CalculateLegalShadowMoves();
    if(!_rules.enabled){
        ClearBlockedCastles();
    }
    FindChecks();
}

bool SlugChess::DoSanMove(const std::string& san_move){
    std::string from = "", to = "";
    if(san_move[0] == '*'){
        _gameEnd = EndResult::StillPlaying;
        return true;
    } else if(isdigit(san_move[0])){
        if(san_move == "1-0"){
            _gameEnd = EndResult::WhiteWin;
        }else if(san_move == "0-1"){
            _gameEnd = EndResult::BlackWin;
        }else if(san_move == "1/2-1/2"){
            _gameEnd = EndResult::Draw;
        } else {
            std::cout << "Error with san move result: " << san_move << std::endl;
            return false;
        }
        return true;
    } else if(san_move[0]=='O' && san_move[1]=='-' && san_move[2]=='O' ){
        auto& legalMoves = _whiteTurn?_legalWhiteMoves:_legalBlackMoves;
        auto& castleFieldes =_whiteTurn?_whiteCastelingOpenFields:_blackCastelingOpenFields;
        bool king_side = !(san_move=="O-O-O");
        for(auto& fields : castleFieldes){
            if(king_side == (fields.back() < fields.front()) ){
                // True if is king_side and King is on a lower rank field than rook
                if(legalMoves.count(fields.back()) > 0 && std::find(legalMoves[fields.back()].begin(), legalMoves[fields.back()].end(), fields.front()) != legalMoves[fields.back()].end()){
                    from = BP(fields.back());
                    to = BP(fields.front());
                }
            }
        }
        
    } else if(isupper(san_move[0])){
        from = std::string(san_move.c_str()+1, 2);
        uint32_t offset = san_move[3] == 'x' || san_move[3] == 'X'?4:3;
        to = std::string(san_move.c_str()+offset, 2);
        
    }else{ //This is a pawn move
        to = std::string(san_move.c_str(), 2);
        //std::cout << to << std::endl;
        if(san_move[2] == 'x' || san_move[2] == 'X'){
            from = to;
            to = std::string(san_move.c_str()+3, 2);
        }else{

            auto func = _whiteTurn?GameRules::DownOne:GameRules::UpOne;
            auto one = func(BPToIndx(to));
            auto two = func(one);
            if(GameRules::LegalPos(one) && Field::IsPawn(_board[one])){
                from = BP(one);
            }
            else if(GameRules::LegalPos(two) && Field::IsPawn(_board[two])){
                from = BP(two);
            }
        }

        //std::cout << from << std::to_string( LegalMove(from, to)) << std::endl;
    }
    if(from != "" && to != "" && LegalMove(from, to)){
        DoMove(from, to);
        return true;
    }
    else
    {
        std::cout << "Error when: " << from << " " << to << std::endl;
        return false;
    }
}

void SlugChess::WriteMoveSan(const std::string& fromStr, const std::string& toStr){
    int from = GameRules::BoardPosToIndex(fromStr);
    int to = GameRules::BoardPosToIndex(toStr);
    int modTo = to%8;
    int modFrom = from%8;
    if(_whiteTurn) _sanMoves << std::to_string(_turn) << ". ";
    //_sanMoves << " ";
    if(Field::IsPawn(_board[from]))
    {
        if(Field::IndexColumn(from) == Field::IndexColumn(to))
        {
            _sanMoves << GameRules::BoardPos(to);
        }else{
            _sanMoves << GameRules::BoardPos(from) << "x" << GameRules::BoardPos(to);
        }
        if((_whiteTurn && Field::IndexRow(to) == 7) ||(!_whiteTurn && Field::IndexRow(to) == 0)){
            _sanMoves << "=Q";
        }
    }
    else if(Field::IsKing(_board[from]) && _board[from].FirstMove && (modTo > modFrom+1 || modTo < modFrom-1))
    {
        if(modTo > modFrom+1){
            _sanMoves << "O-O";
        }
        else if(modTo < modFrom-1)
        {
            _sanMoves << "O-O-O";
        }
    }
    else
    {
        _sanMoves << Field::PieceCharCapital(_board[from].Piece);
        _sanMoves << GameRules::BoardPos(from) << (_board[to].Piece != ChessPiece::Non?"x":"") << GameRules::BoardPos(to);
    }
    
    _sanMoves << " ";
}

void SlugChess::PrintSanMoves(std::stringstream& ss)
{
    ss << _sanMoves.str() << " " << ResultString();
}

void SlugChess::PrintLicense(std::stringstream &ss)
{
    ss << license_text;
}

std::string SlugChess::From(Perspective perspective)
{
    if(_moves.empty()) return "";
    int from = _moves.back().first;
    auto vis = VisionBoardPerspective(perspective);
    return vis[from]?BP(from):"";
}
std::string SlugChess::To(Perspective perspective)
{
    if(_moves.empty()) return "";
    int to = _moves.back().second;
    auto vis = VisionBoardPerspective(perspective);
    return vis[to]?BP(to):"";
}

bool* SlugChess::VisionBoardPerspective(Perspective perspective)
{
    switch (perspective)
    {
    case Perspective::Both:
        return visionBoardTrue;
    case Perspective::White:
        return _whiteVision;
    case Perspective::Black:
        return _blackVision;

    }
    return nullptr;
}

std::string SlugChess::license_text = R"(                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Lesser General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

                    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

                            NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2022 Spaceslug ()

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.
)";